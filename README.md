# OpenML dataset: visualizing_environmental

https://www.openml.org/d/678

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: Unknown - Date unknown  
**Please cite**:   

This S dump contains 22 data sets from the
book Visualizing Data published by
Hobart Press (books@hobart.com).
The dump was created by data.dump()
and can be read back into S by data.restore().
The name of each S data set is the name of
the data set used in the book. To find the
description of the data set in the book look
under the entry - data, name - in the index.
For example, one data set is barley.
To find the description of barley, look
in the index under the entry - data, barley.

File: ../data/visualizing/environmental.csv


Information about the dataset
CLASSTYPE: numeric
CLASSINDEX: none specific

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/678) of an [OpenML dataset](https://www.openml.org/d/678). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/678/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/678/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/678/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

